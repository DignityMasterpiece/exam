const helper = require('./helper');
console.time('output')
console.info(helper([
  {
    "name": "Accessories",
    "id": 1,
    "parent_id": 20,
  },
  {
    "name": "calcetines",
    "id": 2,
    "parent_id": 20,
  },
  {
    "name": "botas",
    "id": 9,
    "parent_id": 20,
  },
  {
    "name": "corbatas",
    "id": 100,
    "parent_id": 5,
  },
  {
    "name": "traje",
    "id": 5,
    "parent_id": 20,
  },
  {
    "name": "Watches",
    "id": 57,
    "parent_id": 1
  },
  {
    "name": "Men",
    "id": 20,
    "parent_id": null,
  },
  {
    "name": "agujetas",
    "id": 45,
    "parent_id": 9,
  },
  {
    "name": "dog toy",
    "id": 112,
    "parent_id": 45,
  },
  {
    "name": "dogs",
    "id": 45,
    "parent_id": null,
  }
], 'parent_id'))
console.timeEnd('output')
