let ordened = []
let data = []

const indexingBy = (array, key) => array.reduce((acc, el) => {
  acc[el[key]] = el
  return acc
}, {})

const getSiblings = (object, key, value) => Object.keys(object)
  .filter(d => object[d][key] === value)
  .map((e) => object[e]);

/**
* This code sorts categories starting with the id lowest to highest beetween they, their subcategories
* @param {Object} disordered This is an disordered categories object
* @param {Object} groupBy This is a property that you need to group siblings
* @param {Object} key The initial value that is needed to compare in the grouping
* @return {Object} this object contain all categories followed by their subcategories
*/
const sorter = (disordered, groupBy, key = null) => {
  if (key === null)
    data = indexingBy(disordered, 'id')
  const siblings = getSiblings(data, groupBy, key)
  for (let i = 0; i < siblings.length; i++) {
    ordened.push(siblings[i])
    sorter(null, groupBy, siblings[i].id)
    delete data[siblings[i].id];
  }
  return ordened
}

module.exports = sorter